FROM awslab

WORKDIR /app

COPY target/crudSpringApp-0.0.1-SNAPSHOT.jar .

EXPOSE 8002

ENTRYPOINT [ "java", "-jar", "crudSpringApp-0.0.1-SNAPSHOT.jar" ]
