package com.ftcarneiro.crudSpring.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ftcarneiro.crudSpring.dto.ProdutoDto;
import com.ftcarneiro.crudSpring.model.Produto;
import com.ftcarneiro.crudSpring.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepo;
	
	
	public ProdutoService() {
		
	}
	
	
	/*
	 * Return object by idProduto
	 */
	public Optional<Produto> getById(Integer idProduto){
		return this.produtoRepo.findById(idProduto);
	}
	
	public List<Produto> buscar(Integer pageNumber, Integer sizeElements) {
		//#AJUSTANDO A NUMERAÇÃO LITERAL DAS PÁGINAS.
		pageNumber--;
		Pageable page = PageRequest.of(pageNumber, sizeElements, Sort.by(Direction.ASC, "nome"));
		Page<Produto> pageProdutos = this.produtoRepo.findAll(page);
		
		return pageProdutos.getContent();
	}
	
	@Transactional(readOnly = false)
	public ProdutoDto salvar(ProdutoDto produtoDto) throws Exception {
		Produto produto = null;
		
		if( produtoDto.getId() == 0 ) {
			produto = new Produto();
		}else {
			produto = this.produtoRepo.getOne(produtoDto.getId());
			if ( produto == null)
				throw new Exception("Produto inválido");
		}

		produto.setDescricao(produtoDto.getDescricao());
		produto.setNome(produtoDto.getNome());
		produto.setValor(produtoDto.getValor());

		this.produtoRepo.save(produto);
		
		produtoDto.load(produto);
		
		return produtoDto;
	}
	
	public String excluir(Integer idProduto) throws Exception {
		Optional<Produto> produto = this.produtoRepo.findById(idProduto);
		
		String message = "Produto não existe.";
		
		if (!produto.isPresent()) {
			throw new Exception(message);
		}else {
			message = String.format("Produto %s exluído.", produto.get().getNome());
			this.produtoRepo.deleteById(idProduto);
		}
		
		return message;
	}
}
