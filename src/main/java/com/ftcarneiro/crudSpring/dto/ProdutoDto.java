package com.ftcarneiro.crudSpring.dto;

import java.math.BigDecimal;

import com.ftcarneiro.crudSpring.model.Produto;

public class ProdutoDto {

	private Integer id;
	
	private String nome;
	
	private String descricao;
	
	private BigDecimal valor;

	public ProdutoDto() {
		
	}
	
	public ProdutoDto(Produto produto) {
		this.load(produto);
	}
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	public void load(Produto produto) {
		this.id = produto.getId();
		this.descricao = produto.getDescricao();
		this.nome = produto.getNome();
		this.valor = produto.getValor();
	}
	
}
