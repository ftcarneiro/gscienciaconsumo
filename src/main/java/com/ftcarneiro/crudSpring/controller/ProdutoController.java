package com.ftcarneiro.crudSpring.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ftcarneiro.crudSpring.dto.ProdutoDto;
import com.ftcarneiro.crudSpring.service.ProdutoService;


/**
 * @apiNote API para manter Produto
 * @author Fabio Tadeu 
 */
@RestController
@RequestMapping("/api/produtos")
public class ProdutoController {

	@Autowired
	private ProdutoService produtoService; 
	
	@GetMapping(produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<ProdutoDto>> busca(Integer page, Integer size) {
		return ResponseEntity.ok(this.produtoService.buscar(page, size).stream().map(produto -> new ProdutoDto(produto)).collect(Collectors.toList()));

	}
	
	
	@GetMapping("/{idProduto}")
	public ResponseEntity<ProdutoDto> recupera(@PathVariable(required = true) Integer idProduto) {
		try {
			ProdutoDto produtoDto = new ProdutoDto(this.produtoService.getById(idProduto).get());
			return new ResponseEntity<>(produtoDto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@PostMapping
	public ResponseEntity<ProdutoDto> cadastra(@RequestBody ProdutoDto produtoDto) throws Exception {

		produtoDto.setId(0);
		produtoDto = this.produtoService.salvar(produtoDto);	

		return new ResponseEntity<>(produtoDto, HttpStatus.OK );
	}
	
	@PutMapping
	public ResponseEntity<ProdutoDto> atualiza(@RequestBody ProdutoDto produtoDto) {

		try {
			produtoDto = this.produtoService.salvar(produtoDto);
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(produtoDto, HttpStatus.OK );
	}

	@DeleteMapping("/{idProduto}")
	public ResponseEntity<String> exlui(@PathVariable(required = true) Integer idProduto){
		String message = null;
		try 
		{
			message = this.produtoService.excluir(idProduto);
		}
		catch (Exception e) 
		{
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(message, HttpStatus.OK);
	}

}
