package com.ftcarneiro.crudSpring.model.filter;

import org.springframework.data.jpa.domain.Specification;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Or;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;

import com.ftcarneiro.crudSpring.model.Produto;

@Or({
    @Spec(path = "nome", params = "busca", spec = Like.class),
    @Spec(path = "descricao", params = "busca", spec = Like.class),
})
public interface ProdutoFiltro extends Specification<Produto> {

}
